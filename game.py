# The print_board function takes a list of
# nine values and prints them in a "pretty"
# 3x3 board
from random import randint

board_numbers = list(range(1,10)) # name change: board as flat list to differentiate betwn 2d
def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in board:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def select_first_player():
  player = randint(0, 1)
  if player == 0:
    print("First player is 'O'")
    return 'O'
  else:
    print("First player is 'X'")
    return 'X'


def switch_player(current_player):
  if current_player == 'O':
    return 'X'
  else:
    return 'O'

def generate_move_question(player): # generate_move_question
  return input('Where would ' + player + ' like to move?')

def check_win(array_of_moves): # could do this with loops, class 'board_matrix'
  if array_of_moves[0] == array_of_moves[1] == array_of_moves[2]:
    return array_of_moves[0]
  elif array_of_moves[3] == array_of_moves[4] == array_of_moves[5]:
    return array_of_moves[3]
  elif array_of_moves[6] == array_of_moves[7] == array_of_moves[8]:
    return array_of_moves[6]
  elif array_of_moves[0] == array_of_moves[3] == array_of_moves[6]:
    return array_of_moves[0]
  elif array_of_moves[1] == array_of_moves[4] == array_of_moves[7]:
    return array_of_moves[1]
  elif array_of_moves[2] == array_of_moves[5] == array_of_moves[8]:
    return array_of_moves[3]
  elif array_of_moves[0] == array_of_moves[4] == array_of_moves[8]:
    return array_of_moves[0]
  elif array_of_moves[2] == array_of_moves[4] == array_of_moves[6]:
    return array_of_moves[2]

def play_game():
  print_board(board_numbers)
  current_player = select_first_player()
  number_of_moves = range(9) #could change this and create a while loop below.
  count_of_moves = 0
  winner = None

  for n in number_of_moves:
    current_move = generate_move_question(current_player)
    if current_move == 'exit':
      print('Exiting game.')
      exit()
    current_move = int(current_move)
    count_of_moves += 1
    board_numbers[current_move - 1] = current_player
    print_board(board_numbers)
    current_player = switch_player(current_player)

    if count_of_moves > 3:
      winner = check_win(board_numbers)
      if winner is not None:
        print('Player ', winner, ' is the winner!')
        exit()

  print("This game is a draw!")



play_game()


